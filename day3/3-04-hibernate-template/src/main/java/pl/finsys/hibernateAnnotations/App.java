package pl.finsys.hibernateAnnotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.finsys.hibernateAnnotations.model.Stock;
import pl.finsys.hibernateAnnotations.service.StockService;

public class App {
    public static void main( String[] args ) {

        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	
    	StockService stockBo = (StockService)appContext.getBean("stockService");
    	
    	/** insert **/
    	Stock stock = new Stock();
    	stock.setStockCode("7668");
    	stock.setStockName("MSFT");
    	stockBo.save(stock);
    	
    	/** select **/
    	Stock stock2 = stockBo.findByStockCode("7668");
    	System.out.println(stock2);
    	
    	/** update **/
    	stock2.setStockName("AAPL");
    	stockBo.update(stock2);
    	
    	/** delete **/
    	stockBo.delete(stock2);
    	
    	System.out.println("Done");
    }
}
