package pl.finsys.hibernateAnnotations.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.finsys.hibernateAnnotations.dao.StockDao;
import pl.finsys.hibernateAnnotations.model.Stock;

@Service("stockService")
public class StockServiceImpl implements StockService {
	
	@Autowired private StockDao stockDao;
	
	public void save(Stock stock){
		stockDao.save(stock);
	}
	
	public void update(Stock stock){
		stockDao.update(stock);
	}
	
	public void delete(Stock stock){
		stockDao.delete(stock);
	}
	
	public Stock findByStockCode(String stockCode){
		return stockDao.findByStockCode(stockCode);
	}
}
