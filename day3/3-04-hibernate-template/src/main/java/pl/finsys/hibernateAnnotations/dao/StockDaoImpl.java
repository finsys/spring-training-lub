package pl.finsys.hibernateAnnotations.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import pl.finsys.hibernateAnnotations.model.Stock;

@Repository("stockDao")
@Transactional
public class StockDaoImpl implements StockDao  {

    private HibernateTemplate template;

    @Autowired
    public StockDaoImpl(SessionFactory sessionFactory) {
        this.template = new HibernateTemplate(sessionFactory);
    }

    @Override
    public void save(Stock stock) {
        template.save(stock);
    }

    @Override
    public void update(Stock stock) {
        template.update(stock);
    }

    @Override
    public void delete(Stock stock) {
        template.delete(stock);
    }

    public Stock findByStockCode(String stockCode) {
        List list = template.find("from Stock where stockCode=?", stockCode);
        return (Stock) list.get(0);
    }

}