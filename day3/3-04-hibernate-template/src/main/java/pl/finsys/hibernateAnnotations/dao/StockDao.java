package pl.finsys.hibernateAnnotations.dao;

import pl.finsys.hibernateAnnotations.model.Stock;

public interface StockDao {
	
	void save(Stock stock);

	void update(Stock stock);

	void delete(Stock stock);

	Stock findByStockCode(String stockCode);

}
