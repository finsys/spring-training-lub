package pl.finsys.hibernateAnnotations.service;

import pl.finsys.hibernateAnnotations.model.Stock;

public interface StockService {
	
	void save(Stock stock);
	
	void update(Stock stock);
	
	void delete(Stock stock);
	
	Stock findByStockCode(String stockCode);

}
