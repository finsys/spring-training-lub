package pl.finsys.jdbcTemplate.customer.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import pl.finsys.jdbcTemplate.customer.dao.CustomerDAO;
import pl.finsys.jdbcTemplate.customer.model.Customer;
import pl.finsys.jdbcTemplate.customer.model.CustomerParameterizedRowMapper;


public class SimpleJdbcCustomerDAO extends SimpleJdbcDaoSupport implements CustomerDAO {
    //insert example
    public void insert(Customer customer) {

        String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";

        getSimpleJdbcTemplate().update(sql, customer.getCustId(), customer.getName(), customer.getAge());
    }

    //insert with named parameter
    public void insertNamedParameter(Customer customer) {

        String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (:custId, :name, :age)";

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("custId", customer.getCustId());
        parameters.put("name", customer.getName());
        parameters.put("age", customer.getAge());

        getSimpleJdbcTemplate().update(sql, parameters);

    }


    //insert batch example
    public void insertBatch(final List<Customer> customers) {

        String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";

        List<Object[]> parameters = new ArrayList<>();
        for (Customer cust : customers) {
            parameters.add(new Object[]{cust.getCustId(), cust.getName(), cust.getAge()});
        }
        getSimpleJdbcTemplate().batchUpdate(sql, parameters);

    }

    //insert batch with named parameter
    public void insertBatchNamedParameter(final List<Customer> customers) {

        String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (:custId, :name, :age)";

        List<SqlParameterSource> parameters = new ArrayList<>();
        for (Customer cust : customers) {
            parameters.add(new BeanPropertySqlParameterSource(cust));
        }

        getSimpleJdbcTemplate().batchUpdate(sql, parameters.toArray(new SqlParameterSource[parameters.size()]));
    }

    //insert batch with named parameter
    public void insertBatchNamedParameter2(final List<Customer> customers) {

        SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(customers.toArray());
        getSimpleJdbcTemplate().batchUpdate("INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (:custId, :name, :age)", params);
    }

    //insert batch example with SQL
    public void insertBatchSQL(final String sql) {
        getJdbcTemplate().batchUpdate(new String[]{sql});

    }

    //query single row with ParameterizedRowMapper
    public Customer findByCustomerId(int custId) {

        String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";

        return getSimpleJdbcTemplate().queryForObject(sql, new CustomerParameterizedRowMapper(), custId);
    }

    //query single row with ParameterizedBeanPropertyRowMapper (Customer.class)
    public Customer findByCustomerId2(int custId) {

        String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";

        return getSimpleJdbcTemplate().queryForObject(sql, ParameterizedBeanPropertyRowMapper.newInstance(Customer.class), custId);
    }

    //query mutiple rows with ParameterizedBeanPropertyRowMapper (Customer.class)
    public List<Customer> findAll() {

        String sql = "SELECT * FROM CUSTOMER";

        return getSimpleJdbcTemplate().query(sql, ParameterizedBeanPropertyRowMapper.newInstance(Customer.class));
    }

    //query mutiple rows with ParameterizedBeanPropertyRowMapper (Customer.class)
    public List<Customer> findAll2() {

        String sql = "SELECT * FROM CUSTOMER";

        return getSimpleJdbcTemplate().query(sql, ParameterizedBeanPropertyRowMapper.newInstance(Customer.class));
    }

    public String findCustomerNameById(int custId) {

        String sql = "SELECT NAME FROM CUSTOMER WHERE CUST_ID = ?";

        return getSimpleJdbcTemplate().queryForObject(sql, String.class, custId);
    }

    public int findTotalCustomer() {

        String sql = "SELECT COUNT(*) FROM CUSTOMER";

        return getSimpleJdbcTemplate().queryForInt(sql);
    }


}
