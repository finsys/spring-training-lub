package pl.finsys.hibernate.stock.service;

import pl.finsys.hibernate.stock.dao.StockDao;
import pl.finsys.hibernate.stock.model.Stock;

public class StockServiceImpl implements StockService {

    private StockDao stockDao;

    public void setStockDao(StockDao stockDao) {
        this.stockDao = stockDao;
    }

    public void save(Stock stock) {
        stockDao.save(stock);
    }

    public void update(Stock stock) {
        stockDao.update(stock);
    }

    public void delete(Stock stock) {
        stockDao.delete(stock);
    }

    public Stock findByStockCode(String stockCode) {
        return stockDao.findByStockCode(stockCode);
    }
}
