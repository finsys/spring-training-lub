package pl.finsys.hibernate.stock.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import pl.finsys.hibernate.stock.model.Stock;

@Transactional
public class StockDaoImpl extends HibernateDaoSupport implements StockDao {

    public void save(Stock stock) {
        getHibernateTemplate().save(stock);
    }

    public void update(Stock stock) {
        getHibernateTemplate().update(stock);
    }

    public void delete(Stock stock) {
        getHibernateTemplate().delete(stock);
    }

    public Stock findByStockCode(String stockCode) {
        List list = getHibernateTemplate().find("from Stock where stockCode=?", stockCode);
        return (Stock) list.get(0);
    }

}
