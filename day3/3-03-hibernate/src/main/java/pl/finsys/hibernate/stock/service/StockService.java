package pl.finsys.hibernate.stock.service;

import pl.finsys.hibernate.stock.model.Stock;

public interface StockService {
	
	void save(Stock stock);
	
	void update(Stock stock);
	
	void delete(Stock stock);
	
	Stock findByStockCode(String stockCode);

}
