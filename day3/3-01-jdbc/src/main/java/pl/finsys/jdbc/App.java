package pl.finsys.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.finsys.jdbc.customer.dao.CustomerDAO;
import pl.finsys.jdbc.customer.model.Customer;

public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    	 
        CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
        Customer customer = new Customer(10, "newUser", 20);
        customerDAO.insert(customer);
    	
        Customer customer1 = customerDAO.findByCustomerId(10);
        System.out.println(customer1);
        
    }
}
