package pl.finsys.jdbc.customer.dao;

import pl.finsys.jdbc.customer.model.Customer;

public interface CustomerDAO {
	public void insert(Customer customer);
	public Customer findByCustomerId(int custId);
}




