
DROP DATABASE IF EXISTS `springTraining`;

CREATE DATABASE `springTraining` /*!40100 COLLATE 'utf8_general_ci' */;

USE `springTraining`;

DROP TABLE IF EXISTS `customer`;
CREATE TABLE  `customer` (
  `CUST_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `AGE` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`CUST_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `customer` (`CUST_ID`,`NAME`,`AGE`) VALUES
  (1,'customer1',28),
  (2,'customer2',31),
  (3,'customer3',11);

DROP TABLE IF EXISTS `stock`;

CREATE TABLE `stock` (
  `STOCK_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `STOCK_CODE` varchar(10) NOT NULL,
  `STOCK_NAME` varchar(20) NOT NULL,
  PRIMARY KEY (`STOCK_ID`) USING BTREE,
  UNIQUE KEY `UNI_STOCK_NAME` (`STOCK_NAME`),
  UNIQUE KEY `UNI_STOCK_ID` (`STOCK_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
  `CAR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CAR_COMPANY` varchar(10) NOT NULL,
  `CAR_MODEL` varchar(20) NOT NULL,
  `CAR_PRICE` DOUBLE NOT NULL,
  PRIMARY KEY (`CAR_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `Employee`;

create table Employee (
  id INT NOT NULL,
  fistName VARCHAR(20) default NULL,
  lastName VARCHAR(20) default NULL,
  dept VARCHAR(20) default NULL,
  PRIMARY KEY (id)
);


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `id` int(40) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `users` (`username`,`password`,`enabled`,`id`) VALUES
  ('user1','user1',0x01,3),
  ('user2','user2',0x00,4),
  ('user3','user3',0x00,6),
  ('user4','user4',0x00,7),
  ('user5','user5',0x00,80),
  ('user6','user6',0x00,90);

DROP TABLE IF EXISTS `security_users`;
DROP TABLE IF EXISTS `security_user_roles`;
DROP TABLE IF EXISTS `security_user_attempts`;

CREATE  TABLE security_users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  accountNonExpired TINYINT NOT NULL DEFAULT 1 ,
  accountNonLocked TINYINT NOT NULL DEFAULT 1 ,
  credentialsNonExpired TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (username));

CREATE TABLE security_user_roles (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES security_users (username));

CREATE TABLE security_user_attempts (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(45) NOT NULL,
  attempts varchar(45) NOT NULL,
  lastModified datetime NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO security_users(username,password,enabled) VALUES ('user','123456', true);
INSERT INTO security_users(username,password,enabled) VALUES ('admin','123456', true);

INSERT INTO security_user_roles (username, role) VALUES ('user', 'ROLE_USER');
INSERT INTO security_user_roles (username, role) VALUES ('admin', 'ROLE_ADMIN');
INSERT INTO security_user_roles (username, role) VALUES ('admin', 'ROLE_USER');


DROP USER 'spring'@'localhost';

CREATE USER 'spring'@'localhost' IDENTIFIED BY 'spring';

GRANT ALL PRIVILEGES ON * . * TO 'spring'@'localhost';

