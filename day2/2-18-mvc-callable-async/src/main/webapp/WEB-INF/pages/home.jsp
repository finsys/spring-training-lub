<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<body>
<ul>
    <li>
        <a href="http://localhost:8080/async/callable/response-body">http://localhost:8080/async/callable/response-body</a>
    </li>
    <li>
        <a href="http://localhost:8080/async/callable/view">http://localhost:8080/async/callable/view</a>
    </li>
    <li>
        <a href="http://localhost:8080/async/callable/exception">http://localhost:8080/async/callable/exception</a>
    </li>
    <li>
        <a href="http://localhost:8080/async/callable/custom-timeout-handling">http://localhost:8080/async/callable/custom-timeout-handling</a>
    </li>
</ul>

<li>
    <a href="http://localhost:8080/async/deferred-result/response-body">http://localhost:8080/async/deferred-result/response-body</a>
</li>
<li>
    <a href="http://localhost:8080/async/deferred-result/model-and-view">http://localhost:8080/async/deferred-result/model-and-view</a>
</li>
<li>
    <a href="http://localhost:8080/async/deferred-result/exception">http://localhost:8080/async/deferred-result/exception</a>
</li>
<li>
    <a href="http://localhost:8080/async/deferred-result/timeout-value">http://localhost:8080/async/deferred-result/timeout-value</a>
</li>

</body>
</html>

