package pl.finsys.methodnameResolvers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class Customer2Controller extends MultiActionController {

    private static final Logger logger = LoggerFactory.getLogger(Customer2Controller.class);
	
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("add");
		return new ModelAndView("CustomerPage", "msg","add() method, PropertiesMethodNameResolver");
	}
	
	public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("delete");
		return new ModelAndView("CustomerPage", "msg","delete() method, PropertiesMethodNameResolver");
	}
	
	public ModelAndView update(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("update");
		return new ModelAndView("CustomerPage", "msg","update() method, PropertiesMethodNameResolver");
	}
	
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("list");
		return new ModelAndView("CustomerPage", "msg","list() method, PropertiesMethodNameResolver");
	}
	
}