package pl.finsys.methodnameResolvers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/customerAnnotated")
public class CustomerControllerAnnotated {

    private static final Logger logger = LoggerFactory.getLogger(CustomerControllerAnnotated.class);

    @RequestMapping("add.htm")
    public ModelAndView add(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("add");
        return new ModelAndView("CustomerPage", "msg", "add() method, annotated");
    }

    @RequestMapping("delete.htm")
    public ModelAndView delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("delete");
        return new ModelAndView("CustomerPage", "msg", "delete() method, annotated");

    }

    @RequestMapping("update.htm")
    public ModelAndView update(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("update");
        return new ModelAndView("CustomerPage", "msg", "update() method, annotated");

    }

    @RequestMapping("list.htm")
    public ModelAndView list(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("list");
        return new ModelAndView("CustomerPage", "msg", "list() method, annotated");
    }

}