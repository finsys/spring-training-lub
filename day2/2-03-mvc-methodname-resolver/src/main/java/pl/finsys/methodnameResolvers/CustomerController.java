package pl.finsys.methodnameResolvers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class CustomerController extends MultiActionController {

    private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

    public ModelAndView testaddCustomer(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("add");
        return new ModelAndView("CustomerPage", "msg", "add() method, multiAction");
    }

    public ModelAndView testdeleteCustomer(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("delete");
        return new ModelAndView("CustomerPage", "msg", "delete() method, multiAction");
    }

    public ModelAndView testupdateCustomer(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("update");
        return new ModelAndView("CustomerPage", "msg", "update() method, multiAction");
    }

    public ModelAndView testlistCustomer(HttpServletRequest request, HttpServletResponse response) throws Exception {
        logger.info("list");
        return new ModelAndView("CustomerPage", "msg", "list() method, multiAction");

    }

}