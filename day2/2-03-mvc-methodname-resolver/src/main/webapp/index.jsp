<!DOCTYPE html>

<html>
    <head>
        <link href="/static/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <table>
            <thead>
            <tr>
                <td >Annotated @Controller</td>
            </tr>
            </thead>
            <tr>
                <td><a href="http://localhost:8080/customerAnnotated/add.htm">http://localhost:8080/customerAnnotated/add.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customerAnnotated/delete.htm">http://localhost:8080/customerAnnotated/delete.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customerAnnotated/update.htm">http://localhost:8080/customerAnnotated/update.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customerAnnotated/list.htm">http://localhost:8080/customerAnnotated/list.htm</a></td>
            </tr>
        </table>

        <table>
            <thead>
            <tr>
                <td>MultiActionController with InternalPathMethodNameResolver</td>
            </tr>
            </thead>
            <tr>
                <td><a href="http://localhost:8080/customer/add.htm">http://localhost:8080/customer/add.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer/delete.htm">http://localhost:8080/customer/delete.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer/update.htm">http://localhost:8080/customer/update.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer/list.htm">http://localhost:8080/customer/list.htm</a></td>
            </tr>
        </table>

        <table>
            <thead>
            <tr>
                <td>MultiActionController with PropertiesMethodNameResolver</td>
            </tr>
            </thead>
            <tr>
                <td><a href="http://localhost:8080/customer2/a.htm">http://localhost:8080/customer2/a.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer2/c.htm">http://localhost:8080/customer2/c.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer2/b.htm">http://localhost:8080/customer2/b.htm</a></td>
            </tr>
            <tr>
                <td><a href="http://localhost:8080/customer2/d.htm">http://localhost:8080/customer2/d.htm</a></td>
            </tr>
        </table>

        <div id="logo"></div>
    </body>
</html>